<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/engage/{id}','PagesController@show');
Route::fallback(function(){
    abort(404);
});


Route::get('/api/getItems', 'PagesController@getItems');
Route::get('/api/getProducts', 'PagesController@getProducts');


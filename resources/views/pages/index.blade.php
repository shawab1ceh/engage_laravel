@extends('layouts/app')
 @section('content');
 <main>
    <div class="main">
      <h2>Superior Quality</h2>
  
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, seder do eiusmod
        tempor incididunt ui labor et dolor magna aliqua. Cum tryi sociis natoque
        penatibus et. Lectus magna fringilla urna porttitor ur placo in egestas
        erata asdasd imperdiet .
      </p>
  
      <div class="items">
       @if (count($items)>0)
       @foreach($items as $item)
       <a href="engage/{{$item->id}}">
        <div
          style="background-image:url({{$item->image}});background-size: cover; height: 13rem;"
        >
          <h3>{{$item->title}}</h3>
        </div>
      </a>
       @endforeach
       @else 
       <p> no items in the database </p>
       @endif
      </div>
    
    <h2>World Class Products</h2>
  
    <div class="products">
     
        @if (count($products)>0)
        @foreach($products as $product)
      <div class="product">
        <img src="{{$product->image}}" alt="" />
        <div class="product-body">
          <h5><a href="#">{{$product->title}}</a></h5>
          <p >
            {{$product->body}}
          </p>
        </div>
      </div>
      @endforeach
      @else
      <p>no products in the database</p>
      @endif
    </div>
  </main>
  @endsection

<!DOCTYPE html>
<html>
  <head>
    <title>Engage</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <link rel="stylesheet" href="{{asset('css/app.css')}}" />
  </head>
  <body>
      <header>
          <div class="headerDecorations">
            <h1>
              <i
                style="margin-right:0.5rem;"
                class="fa fa-handshake-o"
                aria-hidden="true"
              ></i
              >Dramatically Engage
            </h1>
            <p>
              Objectively innovative empowered manufactured products whereas
              parallel platforms
            </p>
            <button>Engage Now</button>
          </div>
        </header>
   @yield('content')
  </body>
</html>

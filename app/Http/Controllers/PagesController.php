<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Product;


class PagesController extends Controller
{
    public function index()
    {
        
      
        return "server is running..";
    }
    public function getItems()
    {
        $items = Item::all();
        // $products = Product::all();
      
        return $items;
    }

    public function getProducts()
    {
        // $items = Item::all();
         $products = Product::all();
      
        return $products;
    }


    public function show($id)
    {
        // $item =  Item::findOrFail($id);
        $item =  Item::find($id);
        if(!$item){
          abort(404);
        // abort(403,'id not found');
        }
        return view ('pages.show')->with('item',$item);
    }}
